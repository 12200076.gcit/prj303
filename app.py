from flask import Flask, render_template, request, jsonify
import os
from deeplearning import OCR
import base64

app = Flask(__name__)

BASE_PATH = os.getcwd()
UPLOAD_PATH = os.path.join(BASE_PATH, 'static/upload/')


@app.route('/')
def home():
    return render_template('home.html')


@app.route('/service', methods=['POST', 'GET'])
def service():
    if request.method == 'POST':
        if 'image_name' in request.files:
            upload_file = request.files['image_name']

            filename = upload_file.filename
            path_save = os.path.join(UPLOAD_PATH, filename)
            upload_file.save(path_save)

            file_extension = os.path.splitext(filename)[1].lower()  # Get the file extension
            if file_extension not in ['.jpg', '.jpeg', '.png']:
                error_message = "Unsupported file format. Only JPG, JPEG, and PNG files are supported."
                return render_template('service.html', upload=False, error_message=error_message)

            text = OCR(path_save, filename)

            return render_template('service.html', upload=True, upload_image=filename, text=text)

        elif 'image_data' in request.form:
            image_data = request.form['image_data']
            image_data = image_data.replace("data:image/png;base64,", "")  # Remove header from base64 image data

            # Save the image from base64 data
            filename = "capture.png"
            path_save = os.path.join(UPLOAD_PATH, filename)

            with open(path_save, "wb") as f:
                f.write(base64.b64decode(image_data))

            text = OCR(path_save, filename)

            return render_template('service.html', upload=True, upload_image=filename, text=text)
            # return jsonify({'text': text})  

    return render_template('service.html', upload=False)


@app.route('/about')
def about():
    return render_template('About.html')


if __name__ == "__main__":
    app.run(debug=True)
